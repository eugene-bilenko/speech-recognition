import math
from datetime import datetime

import parsedatetime
import pytz
from collections import defaultdict
from django.conf import settings
from django.utils import timezone

from network.base import BaseAction
from nlp.parsers import EntitiesParser

SESSION_PLANNED_TASKS_NAME = 'planned_tasks'


class ActionBuilder:
    @classmethod
    def build(cls) -> BaseAction:
        return CleanupTasksListAction(
            ShowTasksListAction(
                CurrentTimeAction(
                    TasksPlanningAction(
                        DefaultAction()
                    )
                )
            )
        )


class DefaultAction(BaseAction):
    def do(self, intent: str, entity: dict, **extra):
        return entity.get('responses')


class CurrentTimeAction(BaseAction):
    intent_name = 'CurrentTime'

    def do(self, intent: str, entity: dict, **extra):
        now = timezone.now()
        time = now.strftime('%I:%M %p')
        return [response_template.format(time=time) for response_template in entity['responses']]


class TasksPlanningAction(BaseAction):
    _response_time_format = '%B, %d %I:%M %p'

    intent_name = 'TasksPlanning'
    extra_requirements = ['query', 'request']

    def do(self, intent: str, entity: dict, **extra):
        query = extra['query']
        request = extra['request']
        calendar = parsedatetime.Calendar()
        time, _ = calendar.parseDT(datetimeString=query, tzinfo=pytz.timezone(settings.TIME_ZONE))

        tasks = request.session.get(SESSION_PLANNED_TASKS_NAME, {})
        if len(tasks) == 0:
            tasks = defaultdict(defaultdict)
        timestamp = math.floor(time.timestamp())

        entities_parser = EntitiesParser()
        chunks = entities_parser.get_chunks_sentence(query, 'SUBJECT')

        if len(chunks) == 0:
            return ['I\'m not sure what you want to schedule ...']

        if time < timezone.now():
            return ['Sorry. I can\'t schedule a "{task}" for the past date.'.format(task=chunks[0])]

        tasks[str(timestamp)] = {'text': chunks[0]}
        request.session[SESSION_PLANNED_TASKS_NAME] = dict(tasks)

        return [
            response_template.format(
                time=time.strftime(self._response_time_format),
                task=chunks[0]
            )
            for response_template in entity['responses']
        ]


class ShowTasksListAction(BaseAction):
    intent_name = 'ShowTasksList'
    extra_requirements = ['query', 'request']

    def do(self, intent: str, entity: dict, **extra):
        query = extra['query']
        request = extra['request']
        tasks = request.session.get(SESSION_PLANNED_TASKS_NAME, {})

        calendar = parsedatetime.Calendar()
        time, flag = calendar.parseDT(datetimeString=query, tzinfo=pytz.timezone(settings.TIME_ZONE))
        filtered = []
        tasks_list = ''

        if len(tasks) > 0:
            filtered = [
                '{date}: {text}'.format(date=datetime.fromtimestamp(int(date)).strftime('%B, %d %I:%M %p'), text=task['text'].title())
                for date, task in tasks.items() if flag == 0 or (flag == 1 and datetime.fromtimestamp(int(date)).date() == time.date())
            ]
            tasks_list = '\n' + ('\n'.join(filtered))

        if len(filtered) > 0:
            return [
                response_template.format(tasks=tasks_list)
                for response_template in entity['responses']
            ]
        else:
            return [
                'You have no any plans so far.'
            ]


class CleanupTasksListAction(BaseAction):
    intent_name = 'CleanupTasksList'
    extra_requirements = ['query', 'request']

    def do(self, intent: str, entity: dict, **extra):
        query = extra['query']
        request = extra['request']

        calendar = parsedatetime.Calendar()
        time, flag = calendar.parseDT(datetimeString=query, tzinfo=pytz.timezone(settings.TIME_ZONE))
        tasks = request.session.get(SESSION_PLANNED_TASKS_NAME)
        new_tasks = defaultdict()

        if len(tasks) > 0:
            if flag == 1:  # Only date without time was parsed.
                for timestamp, task in tasks.items():
                    date = datetime.fromtimestamp(int(timestamp)).date()
                    if date != time.date():
                        new_tasks[timestamp] = task
            elif flag > 1:
                return ['I can\'t do it for specific time.']
            else:
                new_tasks = {}

        request.session[SESSION_PLANNED_TASKS_NAME] = dict(new_tasks)
        return entity.get('responses')
