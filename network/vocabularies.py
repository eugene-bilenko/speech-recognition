from network.actions import ActionBuilder
from network.serializers import VocabularySerializer


class IntentResponseVocabulary(VocabularySerializer):
    _vocabulary = {}

    def __init__(self, dictionary: dict = None):
        if dictionary:
            self._vocabulary = dictionary

    def get_responses(self, intent: str, **extra):
        entity = self._vocabulary.get(intent, {'responses': ['I don\'t know what to say ...']})
        action = ActionBuilder.build()
        responses = action(intent, entity, **extra)
        return responses
