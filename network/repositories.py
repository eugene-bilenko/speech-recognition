import os
import random

from network.base import BaseClassifierRepository
from network.converters import SentenceEncoderDecoder
from network.vocabularies import IntentResponseVocabulary


class IntentClassifierRepository(BaseClassifierRepository):
    labels_dump_filename = 'intent-classifier-labels.pickle'
    vocabulary_dump_filename = 'intent-classifier-vocabulary.pickle'
    intent_vocabulary_dump_filename = 'intent-vocabulary.pickle'
    network_dump_filename = 'intent-classifier-network'

    _sentence_converter = SentenceEncoderDecoder(max_document_length=64)
    _response_vocabulary = IntentResponseVocabulary()

    def define_shapes(self):
        self._input_shape = [None, 64]
        self._output_length = 7

    @property
    def intent_vocabulary(self) -> IntentResponseVocabulary:
        return self._response_vocabulary

    @intent_vocabulary.setter
    def intent_vocabulary(self, value):
        self._response_vocabulary = value

    def response_to_intent(self, intent: str, prediction: dict, **extra) -> str:
        if prediction['probability'] < 0.40:
            intent = 'Unknown'
        return random.choice(self.intent_vocabulary.get_responses(intent, **extra))

    def load(self):
        self._response_vocabulary.load(os.path.join(self.storage_path, self.intent_vocabulary_dump_filename))
        super().load()

    def save(self):
        self._response_vocabulary.save(os.path.join(self.storage_path, self.intent_vocabulary_dump_filename))
        super().save()
