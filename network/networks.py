import os
from typing import List

import tensorflow
import tflearn
from tflearn import conv_1d
from tflearn import dropout
from tflearn import embedding
from tflearn import fully_connected
from tflearn import merge
from tflearn import regression
from tflearn.layers.conv import global_max_pool


class ConvolutionalNeuralNetwork:
    _network = None
    _model = None

    def __init__(self, input_shape: List[int], output_length: int):
        self.build_layers(input_shape, output_length)
        self.build_model()

    def build_layers(self, input_shape: list, output_length: int):
        self._network = tflearn.input_data(shape=input_shape, name='input')
        self._network = embedding(self._network, input_dim=10000, output_dim=128)
        branch1 = conv_1d(self._network, 128, 2, padding='valid', activation='sigmoid', regularizer="L2")
        branch2 = conv_1d(self._network, 128, 3, padding='valid', activation='sigmoid', regularizer="L2")
        branch3 = conv_1d(self._network, 128, 4, padding='valid', activation='sigmoid', regularizer="L2")
        self._network = merge([branch1, branch2, branch3], mode='concat', axis=1)
        self._network = tensorflow.expand_dims(self._network, 2)
        self._network = global_max_pool(self._network)
        self._network = dropout(self._network, 0.9)
        self._network = fully_connected(self._network, output_length, activation='softmax')
        self._network = regression(
            self._network,
            optimizer='adam',
            learning_rate=0.001,
            loss='categorical_crossentropy',
            name='target'
        )

    def predict(self, encoded_tokens: List[List[int]]) -> List[float]:
        return self._model.predict(encoded_tokens).flatten()

    def predict_labels(self, encoded_tokens: List[List[int]]) -> List[int]:
        return self._model.predict_label(encoded_tokens).flatten()

    def build_model(self):
        self._model = tflearn.DNN(self._network)

    def train(self, encoded_data: List[List[int]], encoded_labels: List[List[int]], n_epoch: int = 25, validation_set: set = None):
        self._model.fit(
            encoded_data,
            encoded_labels,
            n_epoch=n_epoch,
            validation_set=validation_set,
            show_metric=True,
            run_id="ConvolutionalNeuralNetwork",
            batch_size=64
        )

    def save(self, folder_path: str, model_name: str):
        if not os.path.isdir(folder_path):
            os.makedirs(folder_path, exist_ok=True)
        self._model.save(os.path.join(folder_path, model_name))

    def load(self, folder_path: str, model_name: str):
        self._model.load(os.path.join(folder_path, model_name))
