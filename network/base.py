import os
from abc import ABC, abstractmethod
from typing import List

from django.conf import settings

from network.converters import SentenceEncoderDecoder, LabelEncoderDecoder
from network.networks import ConvolutionalNeuralNetwork


class BaseAction(ABC):
    _next_instance = None

    intent_name = None
    extra_requirements = []

    def __init__(self, next_instance=None):
        self._next_instance = next_instance

    def __call__(self, intent: str, entity: dict, **extra):
        if intent == self.get_intent_name() or self._next_instance is None:
            self.check_requirements(extra)
            result = self.do(intent, entity, **extra)
        else:
            result = None
        if result is None and self._next_instance is not None:
            result = self._next_instance(intent, entity, **extra)
        return result

    @abstractmethod
    def do(self, intent: str, entity: dict, **extra):
        pass

    def get_intent_name(self):
        return self.intent_name

    def get_requirements(self):
        return self.extra_requirements

    def check_requirements(self, extra):
        for argument in self.get_requirements():
            if argument not in extra:
                raise KeyError('{arg} is required for {cls}'.format(arg=argument, cls=self.__class__.__name__))


class BaseClassifierRepository(ABC):
    _instance = None

    _network = None
    _input_shape = [None, 0]
    _output_length = None
    _sentence_converter = SentenceEncoderDecoder()
    _label_converter = LabelEncoderDecoder()

    storage_path = settings.DUMPS_STORAGE_PATH
    labels_dump_filename = ''
    vocabulary_dump_filename = ''
    network_dump_filename = ''

    def __init__(self):
        self.define_shapes()
        self._network = ConvolutionalNeuralNetwork(self._input_shape, self._output_length)

    @abstractmethod
    def define_shapes(self):
        pass

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            instance = cls()
            instance.load()
            cls._instance = instance
        return cls._instance

    @property
    def label_converter(self):
        return self._label_converter

    @label_converter.setter
    def label_converter(self, converter):
        self._label_converter = converter

    def best_prediction(self, sentence):
        return self.predictions(sentence)[0]

    def predictions(self, sentence: str) -> List[dict]:
        encoded = self._sentence_converter.encode(sentence)
        prediction_scores = self._network.predict([encoded])
        prediction_labels = self._network.predict_labels([encoded])
        result = []
        for index in prediction_labels:
            result.append({
                'index': index,
                'probability': prediction_scores[index],
                'label': self._label_converter.decode_by_index(index),
            })
        return result

    def train(self, sentences_with_labels: List[List[str]], n_epoch=25):
        encoded_sentences = []
        encoded_labels = []
        for sentence, label in sentences_with_labels:
            encoded_sentences.append(self._sentence_converter.encode(sentence))
            encoded_labels.append(self._label_converter.encode(label))
        self._network.train(encoded_sentences, encoded_labels, n_epoch=n_epoch)

    def save(self):
        vocabulary_path = os.path.join(self.storage_path, self.vocabulary_dump_filename)
        labels_path = os.path.join(self.storage_path, self.labels_dump_filename)
        self._label_converter.save(labels_path)
        self._sentence_converter.save(vocabulary_path)
        self._network.save(self.storage_path, self.network_dump_filename)

    def load(self):
        vocabulary_path = os.path.join(self.storage_path, self.vocabulary_dump_filename)
        labels_path = os.path.join(self.storage_path, self.labels_dump_filename)
        self._sentence_converter.load(vocabulary_path)
        self._label_converter.load(labels_path)
        self._network.load(self.storage_path, self.network_dump_filename)
