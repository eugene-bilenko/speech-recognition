import pickle


class VocabularySerializer:
    def save(self, file_path: str):
        with open(file_path, 'wb') as file:
            pickle.dump(self._vocabulary, file)

    def load(self, file_path: str):
        with open(file_path, 'rb') as file:
            self._vocabulary = pickle.load(file)
