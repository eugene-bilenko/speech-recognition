import os
from xml.etree import ElementTree as etree

from collections import defaultdict
from django.conf import settings
from django.core.management import BaseCommand

from network.converters import LabelEncoderDecoder
from network.repositories import IntentClassifierRepository
from network.vocabularies import IntentResponseVocabulary


class Command(BaseCommand):
    intents_file_path = 'intents.xml'

    def handle(self, *args, **options):
        # Load and parse xml with all available intents
        xml_intents = etree.parse(os.path.join(settings.BASE_DIR, 'network/files', self.intents_file_path))
        document = xml_intents.getroot()

        unique_intents = set()
        queries_with_intents = []
        responses_vocab = defaultdict()

        for intent in document:
            # Get intent name and save it as unique classifier for CNN
            intent_name = intent.attrib.get('name')
            intent_type = intent.attrib.get('type', 'other')
            unique_intents.add(intent_name)
            # Get all responses for current intent and save to vocabulary with relation to intent
            responses = []
            for response in intent.find('responses'):
                responses.append(response.text.strip())
            responses_vocab[intent_name] = {
                'responses': responses,
            }
            # Get all queries for current intent and save it with current intent
            # and responses into list[list[query: str, intent: str]]
            for query in intent.find('queries'):
                queries_with_intents.append([query.text, intent_name])

        classifier = IntentClassifierRepository()

        classifier.intent_vocabulary = IntentResponseVocabulary(responses_vocab)
        classifier.label_converter = LabelEncoderDecoder(list(unique_intents))

        classifier.train(queries_with_intents, n_epoch=500)

        print("Saving Intent Classifier Labels/Vocabulary/Network to files...")
        classifier.save()
