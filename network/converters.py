from typing import List

import nltk
import re

from tflearn.data_utils import to_categorical, pad_sequences

from network.serializers import VocabularySerializer


class SentenceEncoderDecoder(VocabularySerializer):
    _vocabulary = {}
    _index = 1
    _stemmer = nltk.LancasterStemmer()
    _ignore_pattern = re.compile(r'[0-9\<\>\$\%\@\#\^\&\+\=\|\_\[\]\\\/\"\;\,\.\`\~\t\n]+')
    _stop_words = nltk.corpus.stopwords.words('english')

    default_max_document_length = 128

    def __init__(self, max_document_length: int = None):
        self.max_document_length = max_document_length or self.default_max_document_length

    def get_token_index(self, token: str):
        return self._vocabulary.get(token, 0)

    def add_to_vocabulary(self, token: str) -> bool:
        if not self.is_in_vocabulary(token):
            self._vocabulary[token] = self._index
            self._index += 1
            return True
        return False

    def is_in_vocabulary(self, token: str) -> bool:
        return token in self._vocabulary

    def encode(self, sentence: str) -> List[int]:
        tokens = []
        for token in nltk.word_tokenize(sentence):
            token = self._stemmer.stem(token)
            if len(self._ignore_pattern.findall(token)) > 0 or token in self._stop_words:
                continue
            self.add_to_vocabulary(token)
            tokens.append(self.get_token_index(token))
        return pad_sequences([tokens], maxlen=self.max_document_length).flatten()

    def encode_sentences(self, sentences: List[str]) -> List[int]:
        result = []
        for sentence in sentences:
            result.append(self.encode(sentence))
        return result

    def decode(self, one_hot_tokens: List[int]) -> str:
        raise NotImplementedError("'decode' method not implemented yet.")

    def load(self, file_path: str):
        super().load(file_path)
        self._index = len(self._vocabulary) + 1


class LabelEncoderDecoder(VocabularySerializer):
    _vocabulary = {}

    def __init__(self, labels: List[str] = None):
        if labels is not None:
            nb_classes = len(labels)
            for index, label in enumerate(labels):
                self._vocabulary[label] = to_categorical([index], nb_classes=nb_classes).flatten()

    def is_in_vocabulary(self, token: str) -> bool:
        return token in self._vocabulary

    def encode(self, label: str) -> List[int]:
        return self._vocabulary[label]

    def decode_by_index(self, index: int):
        return [key for key, value in self._vocabulary.items() if value[index] == 1][0]
