from datetime import datetime

from django import http
from django.utils.html import linebreaks
from django.views import generic

from api.repositories import SpeechToTextRepository
from api.tools import FFMPEG
from network.actions import SESSION_PLANNED_TASKS_NAME
from network.repositories import IntentClassifierRepository


class ProcessUploadedFile(generic.edit.ProcessFormView):

    def convert_file(self, file, format):
        ffmpeg = FFMPEG(format)
        converted_file = ffmpeg.run_process(file)
        setattr(converted_file, 'name', file.name)
        return converted_file

    def recognize(self, file, recognizer):
        try:
            if file.seekable():
                file.seek(0)
            recognizer_repo = SpeechToTextRepository(file, recognizer=recognizer)

            result_text = recognizer_repo.recognize()

            return result_text

        except:
            return ''

    def intent_response(self, request, text):
        classifier = IntentClassifierRepository.get_instance()

        best_prediction = classifier.best_prediction(text)

        response = linebreaks(classifier.response_to_intent(
            best_prediction['label'],
            best_prediction,
            query=text,
            request=request
        ))
        extra = {}
        if best_prediction['label'] in ('TasksPlanning', 'CleanupTasksList', 'ShowTasksList',):
            tasks = request.session.get(SESSION_PLANNED_TASKS_NAME, {})
            extra.update({
                'tasks': [
                        {'date': datetime.fromtimestamp(int(date)).strftime('%B, %d %I:%M %p'), 'task': task['text'].title()}
                        for date, task in tasks.items()
                    ]
            })

        response = {
            'request': text,
            'response': response,
            'best_prediction': {
                'label': best_prediction['label'],
                'probability': '%.2f' % (best_prediction['probability'] * 100),
            }
        }
        response.update(extra)

        return response

    def post(self, request, *args, **kwargs):
        self.request = request
        is_voice_recorder = request.POST.get('voice-recorder', False)
        is_uploaded_file = request.POST.get('uploaded-file', False)
        is_raw_text = request.POST.get('raw-text', False)

        text = 'Unknown request type.'

        if (is_uploaded_file or is_voice_recorder):
            if 'voice-file' not in request.FILES:
                return http.JsonResponse({
                    'error': 'File not found',
                })

            uploaded_file = request.FILES['voice-file']

            file = self.convert_file(uploaded_file, 'wav')

            text = self.recognize(file, 'recognize_sphinx')
            # self.recognize(file, 'recognize_google')
        elif is_raw_text:
            text = request.POST.get('raw-text-data')

        response = self.intent_response(request, text)

        return http.JsonResponse(response, safe=False)
