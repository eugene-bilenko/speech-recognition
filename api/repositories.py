import speech_recognition


class SpeechToTextRepository:
    default_recognizer = 'recognize_sphinx'

    def __init__(self, file, recognizer=None, key=None):
        self._file = file
        self._key = key
        self._recognizer = self.get_recognizer()
        self.recognizer = recognizer or self.default_recognizer

    def get_recognizer(self) -> speech_recognition.Recognizer:
        return speech_recognition.Recognizer()

    def get_audio_data(self) -> speech_recognition.AudioData:
        with speech_recognition.AudioFile(self._file) as file:
            audio_data = self._recognizer.record(file)
        return audio_data

    def recognize(self) -> str:
        recognizer = getattr(self._recognizer, self.recognizer)
        if self._key:
            return recognizer(self.get_audio_data(), key=self._key)
        else:
            return recognizer(self.get_audio_data())
