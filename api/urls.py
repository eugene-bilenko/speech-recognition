from django.conf.urls import url

from api import views

urlpatterns = [
    url(r'^api/speech-process/file/$', views.ProcessUploadedFile.as_view(), name='process-uploaded-file'),
]
