import io
import subprocess


class FFMPEG(object):
    def __init__(self, format='wav'):
        # Setup ffmpeg to convert file from stdin and output it to stdout
        self.args = [
            "ffmpeg",
            "-i",
            "-",
            "-ar",
            "16000",
            "-f",
            format,
            "-",
        ]

    def run_process(self, file: io.BytesIO) -> io.BytesIO:
        print(' '.join(self.args))
        proc = subprocess.Popen(self.args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        if file.seekable():
            file.seek(0)
        std = proc.communicate(input=file.read())
        outstream = io.BytesIO(std[0])
        return outstream
