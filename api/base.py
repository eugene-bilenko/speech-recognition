import os
import pickle


class DumpableObject:
    dump_path = ""

    def save(self):
        print('Save %s...' % self.__class__.__name__)
        with open(self.dump_path, 'wb') as file:
            pickle.dump(self.instance, file)

    def load(self):
        print('Load %s...' % self.__class__.__name__)
        with open(self.dump_path, 'rb') as file:
            self.instance = pickle.load(file)

    def is_dump_exists(self) -> bool:
        return os.path.isfile(self.dump_path)
