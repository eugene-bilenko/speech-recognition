(function ($) {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", $.cookie.get('csrftoken'));
        }
    });
})($ || jQuery);