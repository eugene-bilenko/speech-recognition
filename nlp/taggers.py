import os

import nltk
from django.conf import settings
from nltk import SequentialBackoffTagger, UnigramTagger


class CorpusBrownUnigramTagger(UnigramTagger):
    _instance = None

    def __init__(self, backoff=None, cutoff=0, verbose=False):
        train = nltk.corpus.brown.tagged_sents()
        super().__init__(train=train, backoff=backoff, cutoff=cutoff, verbose=verbose)

    @classmethod
    def get_instance(cls, backoff=None):
        if cls._instance is None:
            instance = cls(backoff=backoff)
            cls._instance = instance
        return cls._instance


class POSTagger(SequentialBackoffTagger):
    _instance = None

    _cfg_grammar_path = os.path.join(settings.BASE_DIR, 'nlp/files/task-entity.cfg')

    def __init__(self, backoff=None):
        super().__init__(backoff=backoff)
        self.grammar = nltk.load_parser(self._cfg_grammar_path)

    def choose_tag(self, tokens, index, history):
        try:
            return next(self.grammar.parse([tokens[index]])).label()
        except:
            if self.backoff:
                res = self.backoff.choose_tag(tokens, index, history)
                if res is None:
                    res = nltk.pos_tag([tokens[index]])[0][1]
                return res
            else:
                return ''

    @classmethod
    def get_instance(cls, backoff=None):
        if cls._instance is None:
            instance = cls(backoff=backoff)
            cls._instance = instance
        return cls._instance
