import os
from typing import List

from django.conf import settings
from nltk import RegexpParser, Tree, untag
from nltk.tokenize.treebank import TreebankWordDetokenizer, TreebankWordTokenizer

from nlp.taggers import POSTagger, CorpusBrownUnigramTagger


class EntitiesParser:
    _tokenizer = TreebankWordTokenizer()
    _detokenizer = TreebankWordDetokenizer()
    _tagger = POSTagger.get_instance(CorpusBrownUnigramTagger.get_instance())

    _entities_chunk_parser_path = os.path.join(settings.BASE_DIR, 'nlp/files/task-entity.regexp')

    def __init__(self):
        self._chunk_parser = self.load_chunk_parser()

    def parse(self, sentence: str) -> Tree:
        tokens = self._tokenizer.tokenize(sentence.lower())
        pos_tags = self._tagger.tag(tokens)
        chunks_tree = self._chunk_parser.parse(pos_tags)
        return chunks_tree

    def find_chunks(self, sentence: str, chunk_name: str) -> List[Tree]:
        chunk_name = chunk_name.upper()
        chunks_tree = self.parse(sentence)
        chunks_subtrees = chunks_tree.subtrees(lambda tree: tree.label() == chunk_name)
        return list(chunks_subtrees)

    def get_chunks_sentence(self, sentence: str, chunk_name: str) -> List[str]:
        chunks_trees = self.find_chunks(sentence, chunk_name)
        sentences = [self._detokenizer.detokenize(untag(tree.leaves())) for tree in chunks_trees]
        return sentences

    def load_chunk_parser(self):
        with open(self._entities_chunk_parser_path, 'rt') as file:
            chunk_parser = RegexpParser(file.read())
            return chunk_parser
