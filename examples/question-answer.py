import nltk
import re

messages = [
    {'question': 'Hi', 'answer': 'Hi! Can I help you?'},
    {'question': 'Hi there', 'answer': 'Hi! Can I help you?'},
    {'question': 'Hello', 'answer': 'Hi! Can I help you?'},
    {'question': 'Hello there', 'answer': 'Hi! Can I help you?'},
    {'question': 'Hey!', 'answer': 'Hi! Can I help you?'},
    {'question': 'Hey there!', 'answer': 'Hi! Can I help you?'},
    {'question': 'Where do you live now?', 'answer': 'I live in Alabama.'},
    {'question': 'In which city town do you live now?', 'answer': 'In Millbrook city.'},
    {'question': 'In which country do you live now?', 'answer': 'I live in USA.'},
    {'question': 'What is the capital of Ukraine?', 'answer': 'Kyev is the capital of Ukraine.'},
    {'question': 'What is your name?', 'answer': 'My name is Amelia.'},
    {'question': 'How I can call you?', 'answer': 'Call me Amelia :)'},
]

stemmer = nltk.LancasterStemmer()

def dialogue_act_features(post):
    features = {}
    tags = nltk.pos_tag(nltk.word_tokenize(post))
    for index, (word, tag) in enumerate(tags):
        features['contains({})'.format(stemmer.stem(word))] = True
    return features

featuresets = [(dialogue_act_features(message.get('question')), message.get('answer')) for message in messages]

classifier = nltk.NaiveBayesClassifier.train(featuresets)
classifier.show_most_informative_features(15)
# print(classifier.classify(dialogue_act_features('in what country you live?')))
