import json

import nltk
from nltk.corpus import wordnet


def make_chapter_json(sentence):
    tokens = []

    tag_tuples = nltk.pos_tag(nltk.word_tokenize(sentence))
    for (string, tag) in tag_tuples:
        token = {'word':string, 'pos':tag}
        tokens.append(token)
    return tokens


def is_stopword(string):
    if string.lower() in nltk.corpus.stopwords.words('english'):
        return True
    return False


def is_punctuation(string):
    for char in string:
        if char.isalpha() or char.isdigit():
            return False
    return True


def wordnet_pos_code(tag):
    if tag.startswith('NN'):
        return wordnet.NOUN
    elif tag.startswith('VB'):
        return wordnet.VERB
    elif tag.startswith('JJ'):
        return wordnet.ADJ
    elif tag.startswith('RB'):
        return wordnet.ADV
    else:
        return ''


def wordnet_pos_label(tag):
    if tag.startswith('NN'):
        return "Noun"
    elif tag.startswith('VB'):
        return "Verb"
    elif tag.startswith('JJ'):
        return "Adjective"
    elif tag.startswith('RB'):
        return "Adverb"
    else:
        return tag


def word_sense_disambiguate(word, wn_pos, sentence):
    senses = wordnet.synsets(word, wn_pos)
    if len(senses) == 0:
        return None
    cfd = nltk.ConditionalFreqDist(
               (sense, def_word)
               for sense in senses
               for def_word in sense.definition().split()
               if def_word in sentence)

    best_sense = senses[0] # start with first sense
    for sense in senses:
        if len(cfd[sense]) > 0 and len(cfd[best_sense]) > 0:
            if cfd[sense].max() > cfd[best_sense].max():
                best_sense = sense
    return best_sense.definition()

text = 'show all scheduled tasks'

tokens = nltk.word_tokenize(text)

sentence = make_chapter_json(text)

disambiguates = [word_sense_disambiguate(d['word'], wordnet_pos_code(d['pos']), text) for d in sentence]

print(disambiguates)
