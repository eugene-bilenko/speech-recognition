import nltk

# document = """One way to capture this intuition that distribution (i) is more "fair" than the other two is to invoke the concept of entropy."""
# document = """What is the most likely label for an input that might have one of two values (but we don't know which)?"""
# document = """He roared with me the pail slip down his back"""
# document = """the dog saw a man in the park"""
documents = [
    """what is the time in new york?""",
    """12 a.m. o'clock now""",
]

grammar = """
        PERSON:     {<P.*>}
        VP:         {<V.*|H.*>}
        NP:         {<NN|DT.*>?<NN.*>?<JJ.*|IN>*<NN.*>}
        LOCATION:   {<IN><DT|PERSON>?<NN.*|NP>}
    """
chunk_parser = nltk.RegexpParser(grammar)
lemmatizer = nltk.WordNetLemmatizer()

def chunk_sentence(tagged_sentence):
    result = chunk_parser.parse(tagged_sentence)
    return result

for document in documents:
    tokens = nltk.word_tokenize(document)
    tokens = [lemmatizer.lemmatize(token) for token in tokens]
    tags = nltk.pos_tag(tokens)

    chunked = chunk_sentence(tags)

    print(str(document))
    print(str(chunked))

    # chunked.draw()
