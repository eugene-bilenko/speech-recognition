import nltk
import re

posts = nltk.corpus.nps_chat.xml_posts()
stemmer = nltk.LancasterStemmer()
ignore_pattern = re.compile(r'[0-9\<\>\$\%\@\#\^\&\+\=\|\_\[\]\\\/\"\;\,\`\~\t\n]+')

unknown_tagger = nltk.DefaultTagger('UNK')
tagger = nltk.UnigramTagger(nltk.corpus.brown.tagged_sents(), backoff=unknown_tagger)

def dialogue_act_features(post):
    features = {}
    tags = tagger.tag(nltk.word_tokenize(post))
    for index, (word, tag) in enumerate(tags):
        if len(ignore_pattern.findall(word)) > 0:
            continue
        features['contains({})'.format(stemmer.stem(word))] = True
        features['tag({})'.format(tag.upper())] = True
    return features
#
featuresets = [(dialogue_act_features(post.text), post.get('class')) for post in posts]

custom_posts = []

featuresets += [(dialogue_act_features(post.get('text')), post.get('class')) for post in custom_posts]

size = int(len(featuresets) * 0.1)

train_set, test_set = featuresets[size:], featuresets[:size]

classifier = nltk.NaiveBayesClassifier.train(train_set)

print("Accuracy: %.2f%%" % (nltk.classify.accuracy(classifier, test_set) * 100))

# for post in posts:
#     answer = classifier.classify(dialogue_act_features(post.text))
#     if post.get('class') != answer:
#         print("%s\nPost: %s\nCorrect: %s\t Guess: %s" % ("="*80, post.text, post.get('class'), answer,))

classifier.show_most_informative_features(5)
