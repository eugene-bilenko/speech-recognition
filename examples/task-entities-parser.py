import nltk
from nltk.tokenize.treebank import TreebankWordDetokenizer, TreebankWordTokenizer


class CustomTagger(nltk.SequentialBackoffTagger):
    def __init__(self, backoff=None):
        super().__init__(backoff=backoff)
        self.grammar = nltk.load_parser('../nlp/files/task-entity.cfg')

    def choose_tag(self, tokens, index, history):
        try:
            return next(self.grammar.parse([tokens[index]])).label()
        except:
            return self.backoff.choose_tag(tokens, index, history)


def load_chunker(file_path):
    with open(file_path, 'rt') as file:
        chunker = nltk.RegexpParser(file.read())
        return chunker

text = 'Remind me about appointment with sponsors tomorrow at 8pm.'
# text = 'Remind me about meeting with friends tomorrow at 8pm.'

lemmatizer = nltk.WordNetLemmatizer()

tokenizer = TreebankWordTokenizer()
detokenizer = TreebankWordDetokenizer()

tokens = [token.lower() for token in tokenizer.tokenize(text)]

tagger = CustomTagger(backoff=nltk.UnigramTagger(train=nltk.corpus.brown.tagged_sents()))

chunker = load_chunker('../nlp/files/task-entity.regexp')

tags = tagger.tag(tokens)

chunks = chunker.parse(tags)

tree = next(chunks.subtrees(lambda tree: tree.label() == 'TASK'))

subsentence = detokenizer.detokenize(nltk.untag(tree.leaves()))

print(subsentence)

# chunks.draw()
