import os
import pickle
from collections import defaultdict

import nltk
import re
import tflearn
import numpy as np
import tensorflow as tf
from tflearn import dropout
from tflearn import embedding
from tflearn import fully_connected
from tflearn import regression
from tflearn.data_utils import pad_sequences, to_categorical
from tflearn.layers.conv import global_max_pool, conv_1d

ignore_pattern = re.compile(r'[0-9\<\>\$\%\@\#\^\&\+\=\|\_\[\]\\\/\"\;\,\`\~\t\n]+')

# vocabulary = {}
# classifications = {}


def encode_sentence(tokenized_sentence):
    encoded = []
    for token in tokenized_sentence:
        encoded.append(vocabulary.get(token, 0))
    return encoded


def encode_label(label):
    return classifications[label]


def get_data():
    xml_posts = nltk.corpus.nps_chat.xml_posts()
    stemmer = nltk.LancasterStemmer()
    tokenized_sentences = []
    # raw_words = []
    # raw_classes = []
    for post in xml_posts:
        tokens = []
        for word in nltk.word_tokenize(post.text):
            # Skip any unneeded symbols
            if len(ignore_pattern.findall(word)) > 0:
                continue
            word = stemmer.stem(word)
            tokens.append(word)
            # Build unique list of indexed words
            # if word not in raw_words:
            #     raw_words.append(word)

        tokenized_sentences.append([tokens, post.get('class')])
        # if post.get('class') not in raw_classes:
        #     raw_classes.append(post.get('class'))
        # Build indexed vocabulary
        # for token in tokens:
        #     if token not in vocabulary:
        #         vocabulary[token] = 0
        #     vocabulary[token] = raw_words.index(token)
    # # Build unique indexes for classifications labels
    # nb_classes = len(raw_classes)
    # for key in raw_classes:
    #     if not key in classifications:
    #         classifications[key] = to_categorical([raw_classes.index(key)], nb_classes=nb_classes).flatten()

    return tokenized_sentences


def decode_label(index):
    return [key for key, value in classifications.items() if value[index] == 1][0]


def get_prediction_accuracy(index, predictions):
    return "%.2f" % (predictions[index] * 100)


sentences = get_data()

# with open('../vocabulary.pickle', 'wb') as file:
#     pickle.dump(vocabulary, file)
#
# with open('../classifications.pickle', 'wb') as file:
#     pickle.dump(classifications, file)

with open('../vocabulary.pickle', 'rb') as file:
    vocabulary = pickle.load(file)

with open('../classifications.pickle', 'rb') as file:
    classifications = pickle.load(file)

# Find most longest sentence
input_length = len(max(sentences, key=lambda v: len(v[0]))[0])
class_length = len(classifications)

sentences = np.array(sentences)

test_size = 0.1 # 10% for test data
testing_size = int(test_size*len(sentences))

data = list(sentences[:, 0][:-testing_size])
labels = list(sentences[:, 1][:-testing_size])

test_data = list(sentences[:, 0][-testing_size:])
test_labels = list(sentences[:, 1][-testing_size:])

# Building Convolutional Neural Networks
input_layer = tflearn.input_data(shape=[None, input_length], name='input')

network = embedding(input_layer, input_dim=input_length, output_dim=128)
branch1 = conv_1d(network, 128, 3, padding='valid', activation='sigmoid', regularizer="L2")
branch2 = conv_1d(network, 128, 4, padding='valid', activation='sigmoid', regularizer="L2")
branch3 = conv_1d(network, 128, 5, padding='valid', activation='sigmoid', regularizer="L2")
network = tflearn.merge([branch1, branch2, branch3], mode='concat', axis=1)
network = tf.expand_dims(network, 2)
network = global_max_pool(network)
network = dropout(network, 0.5)
network = fully_connected(network, class_length, activation='softmax')
network = regression(network, optimizer='adam', learning_rate=0.001, loss='categorical_crossentropy', name='target')

# Training
model = tflearn.DNN(network)

encoded_data = [encode_sentence(sequence) for sequence in data]
encoded_labels = [encode_label(sequence) for sequence in labels]

test_encoded_data = [encode_sentence(sequence) for sequence in test_data]
test_encoded_labels = [encode_label(sequence) for sequence in test_labels]

encoded_data = pad_sequences(encoded_data, maxlen=input_length, value=0)
test_encoded_data = pad_sequences(test_encoded_data, maxlen=input_length, value=0)
#
file_path = '../model.test.network'
print(encoded_data)
if os.path.isfile(file_path + '.index'):
    model.load(file_path)
else:
    model.fit(encoded_data, encoded_labels, n_epoch=50, validation_set=(test_encoded_data, test_encoded_labels), show_metric=True, run_id="dense_model", batch_size=32)

    model.save(file_path)

questions = [
    'I\'m totally agree with you',
    'What is your name?',
    'Hello World',
    'Other than with iterating the list and dicts?',
    'any way to implement this?',
    'What time is it?',
    'Have you been to USA?',
]
for question in questions:

    tokens = nltk.word_tokenize(question)
    encoded_tokens = pad_sequences([encode_sentence(tokens)], maxlen=input_length)

    predictions = model.predict(encoded_tokens).flatten()
    prediction_labels = model.predict_label(encoded_tokens).flatten()

    index1 = prediction_labels[0]
    index2 = prediction_labels[1]
    index3 = prediction_labels[2]
    index4 = prediction_labels[3]
    index5 = prediction_labels[4]
    index6 = prediction_labels[5]
    index7 = prediction_labels[6]
    index8 = prediction_labels[7]
    index9 = prediction_labels[8]
    index10 = prediction_labels[9]
    index11 = prediction_labels[10]
    index12 = prediction_labels[11]
    index13 = prediction_labels[12]
    index14 = prediction_labels[13]
    index15 = prediction_labels[14]

    print("""
        Question: %s
        Prediction(1): %s %s%%
        Prediction(2): %s %s%%
        Prediction(3): %s %s%%
        Prediction(4): %s %s%%
        Prediction(5): %s %s%%
        Prediction(6): %s %s%%
        Prediction(7): %s %s%%
        Prediction(8): %s %s%%
        Prediction(9): %s %s%%
        Prediction(10): %s %s%%
        Prediction(11): %s %s%%
        Prediction(12): %s %s%%
        Prediction(13): %s %s%%
        Prediction(14): %s %s%%
        Prediction(15): %s %s%%
    """ % (
        question,
        decode_label(index1),
        get_prediction_accuracy(index1, predictions),
        decode_label(index2),
        get_prediction_accuracy(index2, predictions),
        decode_label(index3),
        get_prediction_accuracy(index3, predictions),
        decode_label(index4),
        get_prediction_accuracy(index4, predictions),
        decode_label(index5),
        get_prediction_accuracy(index5, predictions),
        decode_label(index6),
        get_prediction_accuracy(index6, predictions),
        decode_label(index7),
        get_prediction_accuracy(index7, predictions),
        decode_label(index8),
        get_prediction_accuracy(index8, predictions),
        decode_label(index9),
        get_prediction_accuracy(index9, predictions),
        decode_label(index10),
        get_prediction_accuracy(index10, predictions),
        decode_label(index11),
        get_prediction_accuracy(index11, predictions),
        decode_label(index12),
        get_prediction_accuracy(index12, predictions),
        decode_label(index13),
        get_prediction_accuracy(index13, predictions),
        decode_label(index14),
        get_prediction_accuracy(index14, predictions),
        decode_label(index15),
        get_prediction_accuracy(index15, predictions),
    ))
