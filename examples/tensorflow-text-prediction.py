import random

import tensorflow as tf
import numpy as np
import pickle
import nltk

from nltk.corpus import names
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

lemmatizer = WordNetLemmatizer()
lines = 10000

def create_lexicon():
    lexicon = []
    lexicon += names.words('male.txt')
    lexicon += names.words('female.txt')

    # Lemmatize words
    lexicon = {word.lower()[:-2] for word in lexicon}

    return list(lexicon)

def sample_handling(samples, lexicon, classification):
    featureset = []
    for sample in samples:
        sample = sample.lower()[:-2]
        if sample in lexicon:
            index = lexicon.index(sample)
            featureset.append([[index], classification])

    return featureset

def create_featuresets_and_lables(test_size=0.1):
    lexicon = create_lexicon()
    features = []
    features += sample_handling(names.words('male.txt'), lexicon, [1, 0])
    features += sample_handling(names.words('female.txt'), lexicon, [0, 1])

    random.shuffle(features)

    features = np.array(features)

    testing_size = int(test_size*len(features))

    train_x = list(features[:, 0][:-testing_size])
    train_y = list(features[:, 1][:-testing_size])

    test_x = list(features[:, 0][-testing_size:])
    test_y = list(features[:, 1][-testing_size:])

    return train_x, train_y, test_x, test_y

train_x, train_y, test_x, test_y = create_featuresets_and_lables()

n_nodes_hl1 = 100
n_nodes_hl2 = 100
n_nodes_hl3 = 100

n_classes = 2
batch_size = 100

# height * width
x = tf.placeholder('float', [None, len(train_x[0])])
y = tf.placeholder('float')

def neural_network_model(data):
    hidden_layer_1 = {
        'weights': tf.Variable(tf.random_normal([len(train_x[0]), n_nodes_hl1])),
        'biases': tf.Variable(tf.random_normal([n_nodes_hl1]))
    }
    hidden_layer_2 = {
        'weights': tf.Variable(tf.random_normal([n_nodes_hl1, n_nodes_hl2])),
        'biases': tf.Variable(tf.random_normal([n_nodes_hl2]))
    }
    hidden_layer_3 = {
        'weights': tf.Variable(tf.random_normal([n_nodes_hl2, n_nodes_hl3])),
        'biases': tf.Variable(tf.random_normal([n_nodes_hl3]))
    }
    output_layer = {
        'weights': tf.Variable(tf.random_normal([n_nodes_hl3, n_classes])),
        'biases': tf.Variable(tf.random_normal([n_classes]))
    }

    # (input_data * weights) + biases
    layer_1 = tf.add(tf.matmul(data, hidden_layer_1['weights']), hidden_layer_1['biases'])
    # Activation function
    layer_1 = tf.nn.relu(layer_1)

    # (input_data * weights) + biases
    layer_2 = tf.add(tf.matmul(layer_1, hidden_layer_2['weights']), hidden_layer_2['biases'])
    # Activation function
    layer_2 = tf.nn.relu(layer_2)

    # (input_data * weights) + biases
    layer_3 = tf.add(tf.matmul(layer_2, hidden_layer_3['weights']), hidden_layer_3['biases'])
    # Activation function
    layer_3 = tf.nn.relu(layer_3)

    output = tf.matmul(layer_3, output_layer['weights']) + output_layer['biases']

    return output


def train_neural_network(x, y):
    prediction = neural_network_model(x)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))

    optimizer = tf.train.AdamOptimizer().minimize(cost)

    # cycles feed forward + backprop
    epochs = 10

    with tf.Session() as session:
        session.run(tf.global_variables_initializer())

        for epoch in range(epochs):
            epoch_loss = 0
            i = 0
            while i < len(train_x):
                start = i
                end = i + batch_size
                i += 1

                batch_x = np.array(train_x[start:end])
                batch_y = np.array(train_y[start:end])

                _, c = session.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
                epoch_loss += c
            print('Epoch %s completed out of %s loss: %s' % (epoch + 1, epochs, epoch_loss,))

        correct = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct, 'float'))

        print('Accuracy: %s' % (session.run(accuracy, feed_dict={x: test_x, y: test_y})))

train_neural_network(x, y)
