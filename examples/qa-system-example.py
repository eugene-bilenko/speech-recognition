"""
    Dynamic Memory Network Example
"""
import pickle
import random

import nltk
import tensorflow
import tflearn
import numpy as np
from collections import defaultdict

from django.utils import timezone

KEYWORDS = {
    'quit': '/q',
    'help': '/?',
}


INTENTS = [
    ('What is reality?', 'Reality',),
    ('What is you name?', 'Name',),
    ('How old are you?', 'Age',),
    ('Where do you live?', 'Location',),
    ('How can you help me?', 'Help',),
    ('Which languages do you speak?', 'Language',),
    ('How are you? Are you doing ok?', 'State',),
    ('What time is it?', 'Time',),
    ('What are your hobbies?', 'Hobby',),
    ('What are you?', 'Appearance',),
    ('Can I change your name?', 'Rename',),
]

INTENT_ANSWERS = defaultdict(None)


def intent_answer(intent):
    def callable_wrapper(fn):
        INTENT_ANSWERS[intent] = fn
        return fn
    return callable_wrapper


def lemmatize(sentence):
    return ' '.join([stemmer.lemmatize(word.lower()) for word in nltk.word_tokenize(sentence)])


def create_classification_model(input_shape: list, output_length: int):
    network = tflearn.input_data(shape=input_shape, name='input')
    network = tflearn.embedding(network, input_dim=10000, output_dim=128)
    branch1 = tflearn.conv_1d(network, 128, 3, padding='valid', activation='sigmoid', regularizer="L2")
    branch2 = tflearn.conv_1d(network, 128, 4, padding='valid', activation='sigmoid', regularizer="L2")
    branch3 = tflearn.conv_1d(network, 128, 5, padding='valid', activation='sigmoid', regularizer="L2")
    network = tflearn.merge([branch1, branch2, branch3], mode='concat', axis=1)
    network = tensorflow.expand_dims(network, 2)
    network = tflearn.layers.conv.global_max_pool(network)
    network = tflearn.dropout(network, 0.5)
    network = tflearn.fully_connected(network, output_length, activation='softmax')
    network = tflearn.regression(
        network,
        optimizer='adam',
        learning_rate=0.001,
        loss='categorical_crossentropy',
        name='target'
    )
    return tflearn.DNN(network)



stemmer = nltk.WordNetLemmatizer()
sentences = [(lemmatize(sentence), intent,) for sentence, intent in INTENTS]
np_sentences = np.array(sentences)

train_data = list(np_sentences[:, 0])
train_labels = list(np_sentences[:, 1])
labels = list(set(np_sentences[:, 1]))

labels_len = len(labels)
max_document_length = len(max(train_data, key=lambda v: len(v)))
print("Max document length:", max_document_length)

vocabulary = tflearn.data_utils.VocabularyProcessor(max_document_length=max_document_length)
vocabulary.fit(train_data)
vocabulary.save('dumps/words.vocabulary.pickle')
# vocabulary = tflearn.data_utils.VocabularyProcessor.restore('dumps/words.vocabulary.pickle')

vocabulary_labels = {label: tflearn.data_utils.to_categorical([index], nb_classes=labels_len).flatten()  for index, label in enumerate(labels)}
with open('dumps/labels.vocabulary.pickle', 'wb') as file:
    pickle.dump(vocabulary_labels, file)

# with open('dumps/labels.vocabulary.pickle', 'rb') as file:
#     vocabulary_labels = pickle.load(file)

classifier = create_classification_model([None, max_document_length], labels_len)

classifier.fit(
    list(vocabulary.transform(train_data)),
    [tflearn.data_utils.to_categorical([labels.index(e)], nb_classes=labels_len).flatten() for e in train_labels],
    n_epoch=300,
    batch_size=64,
    show_metric=True
)
classifier.save('dumps/classifier.model')
# classifier.load('dumps/classifier.model')


def decode_by_index(index: int):
    return [key for key, value in vocabulary_labels.items() if value[index] == 1][0]


def predictions(sentence: str):
    sentence = lemmatize(sentence)
    encoded = list(vocabulary.transform([sentence]))
    prediction_scores = classifier.predict(encoded).flatten()
    prediction_labels = classifier.predict_label(encoded).flatten()
    result = []
    for index in prediction_labels:
        result.append({
            'index': index,
            'probability': prediction_scores[index],
            'label': decode_by_index(index),
        })
    return result


@intent_answer('Reality')
def answer_of_reality(sentence):
    return random.choice([
        'The state of things as they actually exist, as opposed to an idealistic or notional idea of them.',
    ])


@intent_answer('Time')
def time(sentence):
    now = timezone.now()
    return 'It is %s' % now.strftime('%I:%M %p')


@intent_answer('Name')
def name(sentence):
    return random.choice([
        'My name is Amelia.',
        'You can call me Amelia.',
        'My creator calls me Amelia.',
    ])


@intent_answer('Age')
def age(sentence):
    return 'It is indecent to ask about it.'


@intent_answer('Location')
def location(sentence):
    return random.choice([
        'In the cloud. Whatever that means.'
    ])


@intent_answer('Help')
def help(sentence):
    return '\n'.join([
        'You can ask me about:'
    ] + list(np.array(INTENTS)[:,0]))


@intent_answer('Language')
def language(sentence):
    return 'English only so far ...'


@intent_answer('State')
def state(sentence):
    return 'I\'m fine, thanks!'


@intent_answer('Hobby')
def hobby(sentence):
    return 'My hobbies is to help you in everything!'


@intent_answer('Appearance')
def appearance(sentence):
    return random.choice([
        'I\'m your percipient pal. Perspicacious but never parsimonious.',
    ])


@intent_answer('Rename')
def rename(sentence):
    return 'Nah. I like being Amelia.'

input_symbol = '>>> '

symbol = input_symbol

input_data = input(symbol)


while input_data != KEYWORDS['quit']:
    if input_data == KEYWORDS['help']:
        for key, option in KEYWORDS.items():
            print('%s - %s' % (key, option))
    else:
        result = predictions(input_data)
        top_result = result[0]
        print('='*80)
        print('\n%s\n' % INTENT_ANSWERS.get(top_result['label'], lambda _: 'I don\'t know what to say...')(input_data))
        # for prediction in result:
        #     print('{index} {label} {probability}'.format(**prediction))

    input_data = input(symbol)
