import tensorflow

from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets('/tmp/data/', one_hot=True)

n_nodes_hl1 = 500
n_nodes_hl2 = 500
n_nodes_hl3 = 500

n_classes = 10
batch_size = 100

# height * width
x = tensorflow.placeholder('float', [None, 784]) # image 28 x 28 px = 784 flat array items
y = tensorflow.placeholder('float')

def neural_network_model(data):
    hidden_layer_1 = {
        'weights': tensorflow.Variable(tensorflow.random_normal([784, n_nodes_hl1])),
        'biases': tensorflow.Variable(tensorflow.random_normal([n_nodes_hl1]))
    }
    hidden_layer_2 = {
        'weights': tensorflow.Variable(tensorflow.random_normal([n_nodes_hl1, n_nodes_hl2])),
        'biases': tensorflow.Variable(tensorflow.random_normal([n_nodes_hl2]))
    }
    hidden_layer_3 = {
        'weights': tensorflow.Variable(tensorflow.random_normal([n_nodes_hl2, n_nodes_hl3])),
        'biases': tensorflow.Variable(tensorflow.random_normal([n_nodes_hl3]))
    }
    output_layer = {
        'weights': tensorflow.Variable(tensorflow.random_normal([n_nodes_hl3, n_classes])),
        'biases': tensorflow.Variable(tensorflow.random_normal([n_classes]))
    }

    # (input_data * weights) + biases
    layer_1 = tensorflow.add(tensorflow.matmul(data, hidden_layer_1['weights']), hidden_layer_1['biases'])
    # Activation function
    layer_1 = tensorflow.nn.relu(layer_1)

    # (input_data * weights) + biases
    layer_2 = tensorflow.add(tensorflow.matmul(layer_1, hidden_layer_2['weights']), hidden_layer_2['biases'])
    # Activation function
    layer_2 = tensorflow.nn.relu(layer_2)

    # (input_data * weights) + biases
    layer_3 = tensorflow.add(tensorflow.matmul(layer_2, hidden_layer_3['weights']), hidden_layer_3['biases'])
    # Activation function
    layer_3 = tensorflow.nn.relu(layer_3)

    output = tensorflow.matmul(layer_3, output_layer['weights']) + output_layer['biases']

    return output


def train_neural_network(x, y):
    prediction = neural_network_model(x)
    cost = tensorflow.reduce_mean(tensorflow.nn.softmax_cross_entropy_with_logits(logits=prediction, labels=y))

    optimizer = tensorflow.train.AdamOptimizer().minimize(cost)

    # cycles feed forward + backprop
    epochs = 10

    with tensorflow.Session() as session:
        session.run(tensorflow.global_variables_initializer())

        for epoch in range(epochs):
            epoch_loss = 0
            for _ in range(int(mnist.train.num_examples / batch_size)):
                # Data, Labels
                epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                _, c = session.run([optimizer, cost], feed_dict={x: epoch_x, y: epoch_y})
                epoch_loss += c
            print('Epoch %s completed out of %s loss: %s' % (epoch, epochs, epoch_loss,))

        correct = tensorflow.equal(tensorflow.argmax(prediction, 1), tensorflow.argmax(y, 1))
        accuracy = tensorflow.reduce_mean(tensorflow.cast(correct, 'float'))

        print('Accuracy: %s' % (session.run(accuracy, feed_dict={x: mnist.test.images, y: mnist.test.labels})))

train_neural_network(x, y)
