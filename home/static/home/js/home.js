(function () {
    function send_data(action, data) {
        $('.loader-icon').show();

        return $.ajax({
            type: 'POST',
            url: action,
            data: data,
            async: true,
            cache: false,
            contentType: false,
            processData: false
        }).always(function () {
            $('.loader-icon').hide();
        });
    }
    
    function done_response(response) {
        var $response_template = $('#api-response');
        var $request_template = $('#api-request');
        var $scheduler_item_template = $('#api-scheduler');
        var $container = $('#analyzed-response-text');
        var $tasks_container = $('#tasks-list');

        $request_template.tmpl(response).prependTo($container);
        $response_template.tmpl(response).prependTo($container);
        if('tasks' in response)
            $tasks_container.empty();
        $scheduler_item_template.tmpl(response).prependTo($tasks_container);
        $('[data-toggle="tooltip"]').tooltip();
    }

    $('[name="raw-text-data"]').on('keypress', function (e) {
        if(e.originalEvent.charCode == 13) {
            e.preventDefault();
            if($(this).val().length > 0)
                $('[data-plugin="raw-text-analyze"]').trigger('click');
            return false;
        }
    });

    $('[data-dialog="file"]').on('click', function () {
        var $form = $(this).parents('form');
        $form.find('[type="file"]').off().on('change', function () {
            $form.trigger('submit');
        }).trigger('click');
        return false;
    });

    $('[data-plugin="raw-text-analyze"]').on('click', function () {
        var form = new FormData();
        var $form = $(this).parents('form');
        var $button = $(this).find('button');
        var $item = $('[name="raw-text-data"]');

        form.append($item.attr('name'), $item.val());
        form.append('raw-text', true);

        $button.prop('disabled', true);

        send_data($form.attr('action'), form).done(done_response).always(function () {
            $button.prop('disabled', false);
        });

        return false;
    });

    $('[data-plugin="ajax-upload"]').on('submit', function () {
        var form = new FormData();
        var $button = $(this).find('button');

        $($(this).find('[type="file"]')).each(function (index, item) {
            form.append(item.name, item.files[0]);
        });
        form.append('uploaded-file', true);

        $button.prop('disabled', true);

        send_data(this.action, form).done(done_response).always(function () {
            $button.prop('disabled', false);
        });

        return false;
    });

    var recorder_api = new Recorder({
        encoderApplication: 2048,
        encoderSampleRate: 16000,
        originalSampleRateOverride: 16000,
        encoderComplexity: 10,
        resampleQuality: 10,
        numberOfChannels: 1,
        encoderPath:'/static/recorderjs/encoderWorker.min.js'
    });

    recorder_api.addEventListener('dataAvailable', function (e) {
        var data = new Blob([e.detail], { type: 'audio/webm' });
        var form = new FormData();

        // Show player with recorded voice data
        var $player = $('#data-player');
        $player.show();
        var blob_url = URL.createObjectURL(data);
        $player.get(0).src = blob_url;

        var $form = $('form');

        form.append('voice-file', data, 'Recorded ' + Date.now());
        form.append('voice-recorder', true);

        send_data($form.attr('action'), form).done(done_response);
    });

    $('[data-plugin="record-audio"]').on('click', function () {
        var $form = $(this).parents('form');

        if(!this.is_recording) {
            recorder_api.initStream().then(function () {
                recorder_api.start();
            });
            this.is_recording = true;
            $(this).addClass('btn-danger').removeClass('btn-success').find('.fa').removeClass('fa-microphone').addClass('fa-stop');
            $form.find('button[type="submit"]').prop('disabled', true);
        } else {
            this.is_recording = false;
            recorder_api.stop();
            $(this).addClass('btn-success').removeClass('btn-danger').find('.fa').removeClass('fa-stop').addClass('fa-microphone');
            $form.find('button[type="submit"]').prop('disabled', false);
        }
        return false;
    });
})();
